import { AppContainer } from 'react-hot-loader';
import * as React from "react"
import { ReactGhLikeDiff } from "react-gh-like-diff";
import "react-gh-like-diff/lib/diff2html.min.css";
const fs=require('fs');

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.diffString=fs.readFileSync("src/test.txt").toString();
    }

    render() {
        return (
                <AppContainer><ReactGhLikeDiff
                    options={{
                        originalFileName: "a",
                        updatedFileName: "b",
                        outputFormat: "side-by-side",
                        showFiles: false
                    }}
                    diffString={this.diffString}
                /></AppContainer>
          );
    }
}
