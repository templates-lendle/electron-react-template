import * as React from "react"
import * as ReactDOM from "react-dom"
import App from './App.js';

console.log('Heya, this is coming from a webpack bundle');
ReactDOM.render(<App/>, document.getElementById("app"));